function Footer() {
    return (
        <footer className="jumbotron bg-orange mt-5 d-flex justify-content-center ">
          <div className="row">
            <div className="col d-flex flex-column justify-content-around text-center">
              <h4>Footer</h4>
              <div className="text-center">
                <a href="#" className="btn btn-dark mt-2"><i className="fa fa-arrow-up" /> &nbsp; To the top</a>
              </div>      
              <div className="text-dark mt-3 mb-2">
                <i className="fab fa-facebook-square" />
                <i className="fab fa-instagram" />
                <i className="fab fa-snapchat" />
                <i className="fab fa-twitter" />
                <i className="fab fa-linkedin-in" />
              </div>
              <b>Powered by DEVCAMP</b>
            </div>
          </div>
        </footer>
    )
}

export default Footer;