import Carousel from 'react-bootstrap/Carousel'
import imgPizza1 from "../../assets/images/pizza (1).jpg";
import imgPizza2 from "../../assets/images/pizza (2).jpg";
import imgPizza3 from "../../assets/images/pizza (3).jpg";
import imgPizza4 from "../../assets/images/pizza (4).jpg";
function PizzaCarousel() {
    return (
        <div className="container">
            <Carousel>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={imgPizza1}
                alt="First slide japanese Okonomiyaki pizza"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={imgPizza2}
                alt="Second slide seafood pizza"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={imgPizza3}
                alt="Third slide mediterranian pizza"
                />
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={imgPizza4}
                alt="Fourth slide tropical seafood pizza"
                />
            </Carousel.Item>
            </Carousel>
        </div>
    )
}
export default PizzaCarousel