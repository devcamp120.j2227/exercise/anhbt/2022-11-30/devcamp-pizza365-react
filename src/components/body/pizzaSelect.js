import pizzaCCB from "../../assets/images/pizza (cheesy chicken bacon).jpg";
import pizzaHawaii from "../../assets/images/pizza (hawaiian).jpg";
import pizzaOcean from "../../assets/images/pizza (ocean mania).jpg";


function PizzaSelect () {
    return (
        <div className="container mt-5">
          <div className="row d-flex justify-content-center">
            <div className="col-sm-4">
              <h2 className="text-center heading-pizza365"> Chọn loại pizza</h2>
            </div>
          </div>
          <div className="d-flex justify-content-around" id="div-pizza-select">
            <div className="card" style={{width: '24rem'}}>
              <img className="card-img-top" src={pizzaOcean} alt="Card image cap" />
              <div className="card-body d-flex flex-column">
                <h5 className="card-title"><b>OCEAN MANIA</b></h5>
                <p className="card-text">PIZZZA HẢI SẢN SỐT MAYONNAISE</p>
                <p>Xốt cà chua, phô mai mozzarella, tôm mực, thanh cua, hành tây</p>
                <button className="btn w-100 btn-warning bg-orange mt-auto btn-select-pizza"><b>Chọn</b></button>
              </div>
            </div>
            <div className="card" style={{width: '24rem'}}>
              <img className="card-img-top" src={pizzaHawaii} alt="Card image cap" />
              <div className="card-body d-flex flex-column">
                <h5 className="card-title"><b>HAWAIIAN</b></h5>
                <p className="card-text">PIZZZA DĂM BÔNG DỨA</p>
                <p>Xốt cà chua, phô mai mozzarella, dăm bông, dứa</p>
                <button className="btn w-100 btn-warning bg-orange mt-auto btn-select-pizza"><b>Chọn</b></button>
              </div>
            </div>
            <div className="card" style={{width: '24rem'}}>
              <img className="card-img-top" src={pizzaCCB } alt="Card image cap" />
              <div className="card-body d-flex flex-column">
                <h5 className="card-title"><b>CHEESY CHICKEN BACON</b></h5>
                <p className="card-text">PIZZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                <p>Xốt phô mai, thịt gà, ba rọi xông khói, cà chua</p>
                <button className="btn w-100 btn-warning bg-orange mt-auto btn-select-pizza"><b>Chọn</b></button>
              </div>
            </div>
          </div>
        </div>
    )
}
export default PizzaSelect