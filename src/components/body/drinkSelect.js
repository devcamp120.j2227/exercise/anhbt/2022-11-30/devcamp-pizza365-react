function DrinkSelect () {
    return (
        <div className="container mt-5">
          <div className="row d-flex justify-content-center">
            <div className="col-sm-4">
              <h2 className="text-center heading-pizza365"> Chọn đồ uống</h2>
            </div>
          </div>
          <div className="input-group mb-3 mt-3">
            <select className="form-control" id="select-drink">
              <option value>Tất cả loại nước uống</option>
            </select>
          </div>
        </div>
    )
}

export default DrinkSelect