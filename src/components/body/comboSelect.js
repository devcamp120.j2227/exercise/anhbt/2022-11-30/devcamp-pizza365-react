function ComboSelect(){
    return (
        <div className="container mt-5">
          <div className="row d-flex justify-content-center">
            <div className="col-sm-4">
              <h2 className="text-center heading-pizza365"> Chọn combo</h2>
            </div>
          </div>
          <div className="row d-flex justify-content-center">
            <p className="text-center text-orange"> Chọn combo phù hợp với nhu cầu của bạn</p>
          </div>
          <div className="d-flex justify-content-around">
            <div className="card" style={{width: '18rem'}}>
              <div className="card-body bg-orange">
                <h4 className="card-title text-center">Small</h4>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item text-center">Đường kính: <b>20cm</b></li>
                <li className="list-group-item text-center">Sườn nướng: <b>2</b></li>
                <li className="list-group-item text-center">Salad: <b>200g</b></li>
                <li className="list-group-item text-center">Nước ngọt: <b>2</b></li>
                <li className="list-group-item text-center"><b><h1>150.000</h1></b><p>VNĐ</p></li>
              </ul>
              <div className="card-footer text-center">
                <button className="btn w-100 btn-warning bg-orange btn-select-combo"><b>Chọn</b></button>
              </div>
            </div>
            <div className="card" style={{width: '18rem'}}>
              <div className="card-body bg-warning">
                <h4 className="card-title text-center">Medium</h4>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item text-center">Đường kính: <b>25cm</b></li>
                <li className="list-group-item text-center">Sườn nướng: <b>4</b></li>
                <li className="list-group-item text-center">Salad: <b>300g</b></li>
                <li className="list-group-item text-center">Nước ngọt: <b>3</b></li>
                <li className="list-group-item text-center"><b><h1>200.000</h1></b><p>VNĐ</p></li>
              </ul>
              <div className="card-footer">
                <button className="btn w-100 btn-warning btn-select-combo"><b>Chọn</b></button>
              </div>
            </div>
            <div className="card" style={{width: '18rem'}}>
              <div className="card-body bg-orange">
                <h4 className="card-title text-center">Large</h4>
              </div>
              <ul className="list-group list-group-flush">
                <li className="list-group-item text-center">Đường kính: <b>30cm</b></li>
                <li className="list-group-item text-center">Sườn nướng: <b>8</b></li>
                <li className="list-group-item text-center">Salad: <b>500g</b></li>
                <li className="list-group-item text-center">Nước ngọt: <b>4</b></li>
                <li className="list-group-item text-center"><b><h1>250.000</h1></b><p>VNĐ</p></li>
              </ul>
              <div className="card-footer">
                <button className="btn w-100 btn-warning bg-orange btn-select-combo"><b>Chọn</b></button>
              </div>
            </div>
          </div>
        </div>
    )
}
export default ComboSelect