function SendOrder () {
    return (
        <div className="container mt-5">
          <div className="row d-flex justify-content-center">
            <div className="col-sm-4">
              <h2 className="text-center heading-pizza365"> Gửi đơn hàng</h2>
            </div>
          </div>
          <form id="customer-info-form">
            <div className="form-group">
              <label>Tên</label>
              <input type="text" className="form-control" placeholder="Nhập tên" id="inp-name" />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input type="text" className="form-control" placeholder="Nhập email" id="inp-email" />
            </div>
            <div className="form-group">
              <label>Số điện thoại</label>
              <input type="text" className="form-control" placeholder="Nhập số điện thoại" id="inp-phone" />
            </div>
            <div className="form-group">
              <label>Địa chỉ</label>
              <input type="text" className="form-control" placeholder="Nhập địa chỉ" id="inp-address" />
            </div>
            <div className="form-group">
              <label>Mã giảm giá</label>
              <input type="text" className="form-control" placeholder="Nhập mã giảm giá" id="inp-voucher" />
            </div>
            <div className="form-group">
              <label>Lời nhắn</label>
              <input type="text" className="form-control" placeholder="Nhập lời nhắn" id="inp-message" />
            </div>
            <button type="button" className="btn w-100 btn-warning bg-orange" id="btn-send-order"><b>Gửi</b></button>
          </form>
        </div>
    )
}
export default SendOrder;