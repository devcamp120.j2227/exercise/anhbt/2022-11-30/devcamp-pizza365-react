function WhyUs () {
    return (
        <div className="container mt-5">
          <div className="row d-flex justify-content-center">
            <div className="col-sm-4">
              <h2 className="text-center heading-pizza365"> Tại sao lại pizza 365</h2>
            </div>
          </div>
          <div className="row d-flex justify-content-center mt-2"> 
            <div className="col border border-warning border-right-0 p-4" style={{backgroundColor: 'lightgoldenrodyellow'}}>
              <div className="container">
                <h3>Đa dạng</h3>
                <p>Số lượng pizza đa dạng, có đầy dủ các loại pizza đang hot nhất hiện nay</p>
              </div>
            </div>
            <div className="col border border-warning border-right-0 p-4" style={{backgroundColor: 'yellow'}}>
              <div className="container">
                <h3>Chất lượng</h3>
                <p>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm</p>
              </div>
            </div>
            <div className="col border border-warning border-right-0 p-4" style={{backgroundColor: 'lightsalmon'}}>
              <div className="container">
                <h3>Hương vị</h3>
                <p>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo bệ sinh an toàn thực phẩm</p>
              </div>
            </div>
            <div className="col border border-warning p-4" style={{backgroundColor: 'orange'}}>
              <div className="container">
                <h3>Dịch vụ</h3>
                <p>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến</p>
              </div>
            </div>
          </div>
        </div>
    )
}

export default WhyUs;