function Navbar() {
    return (
    <nav className="navbar navbar-expand-lg navbar-light bg-orange border-bottom sticky-top">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav nav-fill w-100">
              <li className="nav-item active">
                <a className="nav-link" href="#">Trang chủ <span className="sr-only"></span></a>
              </li>
              <li className="nav-item ">
                <a className="nav-link" href="#">Combo</a>
              </li>
              <li className="nav-item ">
                <a className="nav-link" href="#">Loại Pizza</a>
              </li>
              <li className="nav-item ">
                <a className="nav-link" href="#">Gửi đơn hàng</a>
              </li> 
            </ul>
          </div>
    </nav>)
}

export default Navbar;