

import Navbar from "./components/header/navbar";
import PizzaCarousel from "./components/body/pizzaCarousel";
import WhyUs from "./components/body/whyUs";
import ComboSelect from "./components/body/comboSelect";
import PizzaSelect from "./components/body/pizzaSelect";
import DrinkSelect from "./components/body/drinkSelect";
import SendOrder from "./components/body/sendOrder";
import Footer from "./components/footer/footer";
function App() {
  return (
      <div>
        {/* Header */}
        <Navbar></Navbar>
        {/* Carousel Pizza */}
        <PizzaCarousel></PizzaCarousel>
        {/* Giới thiệu Pizza 365 */}
        <WhyUs></WhyUs>
        {/* Combo Pizza */}
        <ComboSelect></ComboSelect>
        {/* Loại Pizza */}
        <PizzaSelect></PizzaSelect>
        {/* Đồ uống */}
        <DrinkSelect></DrinkSelect>
        {/* Gửi đơn hàng */}
        <SendOrder></SendOrder>
        {/* Footer */}
        <Footer></Footer>
      </div>
    );
  }
  
  export default App;
